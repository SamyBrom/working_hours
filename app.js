
  angular.module('myApp', ['mwl.calendar', 'ui.bootstrap','ngAnimate'])
  .controller('KitchenSinkCtrl', function(moment,$scope) {

    var vm = this;

    $scope.loltest = function() {
      return 'lol';
    }

    $scope.evalDur = function(ms) {
      // 1- Convert to seconds:
      var seconds = ms / 1000;
      // 2- Extract hours:
      var hours = parseInt( seconds / 3600 ); // 3,600 seconds in 1 hour
      seconds = seconds % 3600; // seconds remaining after extracting hours
      // 3- Extract minutes:
      var minutes = parseInt( seconds / 60 ); // 60 seconds in 1 minute
      // 4- Keep only seconds not extracted to minutes:
      seconds = seconds % 60;
      // return( hours+":"+minutes+":"+seconds);
      return( hours+":"+minutes);
      // return ms;
    }

//     $scope.evalDur = function(s) {
//       s= s / 0.001;
//       var ms = s % 1000;
//     s = (s - ms) / 1000;
//     var secs = s % 60;
//     s = (s - secs) / 60;
//     var mins = s % 60;
//     var hrs = (s - mins) / 60;
//
//     return hrs + ':' + mins + ':' + secs + '.' + ms;
// }

    //These variables MUST be set as a minimum for the calendar to work
    vm.calendarView = 'month';
    vm.calendarTitle = 'test';
    vm.viewDate = new Date();
    vm.events = [
      {
        title: 'An event',
        type: 'warning',
        startsAt: moment().startOf('week').subtract(2, 'days').add(8, 'hours').toDate(),
        endsAt: moment().startOf('week').add(1, 'week').add(9, 'hours').toDate(),
        draggable: true,
        resizable: true
      }, {
        title: '<i class="glyphicon glyphicon-asterisk"></i> <span class="text-primary">Another event</span>, with a <i>html</i> title',
        type: 'info',
        startsAt: moment().subtract(1, 'day').toDate(),
        endsAt: moment().add(5, 'days').toDate(),
        draggable: true,
        resizable: true
      }, {
        title: 'This is a really long event title that occurs on every year',
        type: 'important',
        startsAt: moment().startOf('day').add(7, 'hours').toDate(),
        endsAt: moment().startOf('day').add(19, 'hours').toDate(),
        recursOn: 'year',
        draggable: true,
        resizable: true
      }
    ];

    // vm.events.total = moment(vm.events.endsAt) - moment(vm.events.startsAt);
    // vm.events.total = moment.duration(vm.events.endsAt.diff(vm.events.startsAt));


    vm.isCellOpen = true;

    vm.eventClicked = function(event) {
      alert('Clicked');
    };

    vm.eventEdited = function(event) {
      alert('Edited');
    };

    vm.eventDeleted = function(event) {
      alert('Deleted');
    };

    vm.eventTimesChanged = function(event) {
      alert('Dropped or resized');
    };

    vm.toggle = function($event, field, event) {
      $event.preventDefault();
      $event.stopPropagation();
      event[field] = !event[field];
    };

  });
