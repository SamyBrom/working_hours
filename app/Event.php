<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $fillable = [
      'draggable', 'endsAt', 'resizable','startsAt','title','type','user_id',
  ];
  public function post()
  {
      return $this->belongsTo('App\User');
  }
}
