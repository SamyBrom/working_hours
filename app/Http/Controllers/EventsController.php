<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Event;
use App\User;

class EventsController extends Controller
{
    public function index(Request $request)
    {
      $user_id = Auth::user()->id;
      $events = User::find($user_id)->events()->get();
      return $events;
    }

    public function store(Request $request)
    {
      // $validator = Validator::make($request->all(), [
      //   'title' => 'required|max:255',
      // ]);
      // if ($validator->fails()) {
      //     return redirect('/')
      //         ->withInput()
      //         ->withErrors($validator);
      // }

      // dd($request->all());
      $user_id = Auth::user()->id;
      $events = $request->all();
      for ($i=0; $i < count($events ); $i++) {
        $events[$i]['user_id'] = $user_id;
      }


      // dd($events);
      DB::table('events')->insert($events);



      // Event::insert($request->all());
      // return $request->all();


      }

      public function edit(Request $request)
      {

        $user_id = Auth::user()->id;
        $events = $request->all();
        for ($i=0; $i < count($events ); $i++) {
          $events[$i]['user_id'] = $user_id;
        }


        foreach ($events as $event) {
          DB::table('events')
              ->where('id', $event['id'])
              ->update($event);
        }

      }

      public function delete(Request $request)
      {

        $user_id = Auth::user()->id;
        $events = $request->all();
        for ($i=0; $i < count($events ); $i++) {
          $events[$i]['user_id'] = $user_id;
        }


        foreach ($events as $event) {
          DB::table('events')
              ->where('id', $event['id'])
              ->delete();
        }

        // DB::table('events')->truncate();

      }
}
