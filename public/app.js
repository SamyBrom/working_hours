
  var myApp = angular.module('myApp', ['mwl.calendar', 'ui.bootstrap','ngAnimate'])
  .controller('KitchenSinkCtrl', function($http,moment,$scope) {

    $scope.newEvents = [];
    $scope.editedEvents = [];
    $scope.deletedEvents = [];
    $scope.lastId = 0;
    $scope.changed = false;



    var vm = this;

    $scope.changeClass = function(event) {
      if ($.grep($scope.newEvents, function(e){ return e.id === event.id; }).length == 1)
        return 'success';
      else if ($.grep($scope.editedEvents, function(e){ return e.id === event.id; }).length == 1)
        return 'info';
    }

    $scope.deleteEvent = function(event, index) {
      $scope.deletedEvents.push(event);vm.events.splice((vm.events.length-1) - index, 1);
      $scope.changed = true;
    }

    $scope.addNew = function(event, index) {
      vm.events.push({id: vm.events.length+1, title: '', type: 'important', draggable: true, resizable: true, new: true, startsAt: new Date()});
      $scope.changed = true;
    }

    $scope.cacheChange = function(event) {
      $scope.changed = true;
      if (event.new == true ) {
        if ($.grep($scope.newEvents, function(e){ return e.id === event.id; }).length == 0)
          $scope.newEvents.push(event);
        else{
          for(var i = 0; i < $scope.newEvents.length; i++) {
            if($scope.newEvents[i].id == event.id) {
                $scope.newEvents.splice(i, 1);
                break;
            }
          }
          $scope.newEvents.push(event);
        }
      }
      else {
        if ($.grep($scope.editedEvents, function(e){ return e.id === event.id; }).length == 0) {
            $scope.editedEvents.push(event);
        }
        else {
            for(var i = 0; i < $scope.editedEvents.length; i++) {
              if($scope.editedEvents[i].id == event.id) {
                  $scope.editedEvents.splice(i, 1);
                  break;
              }
            }
            $scope.editedEvents.push(event);
        }
      }
    }


    $scope.saveChanges = function() {
      var failed = false;
      if ($scope.newEvents.length > 0) {
        for (var i = 0; i < $scope.newEvents.length; i++) {
          delete $scope.newEvents[i].new;
          delete $scope.newEvents[i].id;
          delete $scope.newEvents[i].endOpen;
          delete $scope.newEvents[i].startOpen;
        }
        console.log('Inserting: '+$scope.newEvents);
        $http({
                method: 'POST',
                url: 'api/storetasks',
                data: $scope.newEvents,
                // headers: {'Content-Type': 'application/x-www-form-urlencoded'}
              }).then(function successCallback(response) {
                  // this callback will be called asynchronously
                  // when the response is available
                  console.log('Success: '+ response);
                  toastr.success('New times added successfully');
                }, function errorCallback(response) {
                  // called asynchronously if an error occurs
                  // or server returns response with an error status.
                  console.log('Error: '+ response);
                  toastr.warning('New times insert failed, please contact Sami');
                  failed = true;
                });
        }
        if ($scope.editedEvents.length > 0) {
          console.log('Editing: '+$scope.editedEvents);
          $http({
                  method: 'POST',
                  url: 'api/edittasks',
                  data: $scope.editedEvents,
                  // headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function successCallback(response) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log('Success: '+ response);
                    toastr.success('Times edited successfully');
                  }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    console.log('Error: '+ response);
                    toastr.warning('Time edit failed, please contact Sami');
                    failed = true;
                  });
          }
          if ($scope.deletedEvents.length > 0) {
            console.log('Deleting: '+$scope.editedEvents);
            $http({
                    method: 'POST',
                    url: 'api/deletetasks',
                    data: $scope.deletedEvents,
                    // headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                  }).then(function successCallback(response) {
                      // this callback will be called asynchronously
                      // when the response is available
                      console.log('Success: '+ response);
                      toastr.success('Times deleted successfully');
                    }, function errorCallback(response) {
                      // called asynchronously if an error occurs
                      // or server returns response with an error status.
                      console.log('Error: '+ response);
                      toastr.warning('Time deletion failed, please contact Sami');
                      failed = true;
                    });
            }

            if (!failed) {
              $scope.newEvents = [];
              $scope.editedEvents = [];
              $scope.deletedEvents = [];
              $scope.changed = false;
            }
    }


    $scope.loltest = function() {
      return 'lol';
    }

    $scope.evalDur = function(ms) {
      // 1- Convert to seconds:
      var seconds = ms / 1000;
      // 2- Extract hours:
      var hours = parseInt( seconds / 3600 ); // 3,600 seconds in 1 hour
      seconds = seconds % 3600; // seconds remaining after extracting hours
      // 3- Extract minutes:
      var minutes = parseInt( seconds / 60 ); // 60 seconds in 1 minute
      // 4- Keep only seconds not extracted to minutes:
      seconds = seconds % 60;
      // return( hours+":"+minutes+":"+seconds);
      var result = hours+":"+minutes 
      if (result.indexOf('-')==0)
        return 'Enter end time to show total';
      else
        return result;
      // return ms;
    }

//     $scope.evalDur = function(s) {
//       s= s / 0.001;
//       var ms = s % 1000;
//     s = (s - ms) / 1000;
//     var secs = s % 60;
//     s = (s - secs) / 60;
//     var mins = s % 60;
//     var hrs = (s - mins) / 60;
//
//     return hrs + ':' + mins + ':' + secs + '.' + ms;
// }

  $http({
    method: 'GET',
    url: 'api/gettasks',
    // headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }).then(function successCallback(response) {
      // this callback will be called asynchronously
      // when the response is available
      console.log('Success: '+ response);
      toastr.success('Loaded times successfuly');
      vm.events = response.data;
      for(var k = 0, length3 = vm.events.length; k < length3; k++){
        vm.events[k].startsAt = new Date(vm.events[k].startsAt);
        vm.events[k].endsAt = new Date(vm.events[k].endsAt);
      }
      vm.totalHours = 0;

      vm.events.forEach( function(event, index) {
        vm.totalHours += (event.endsAt - event.startsAt) * 0.000000277778;
      });
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      console.log('Error: '+ response);
      toastr.warning('Error loading times');
    });

    //These variables MUST be set as a minimum for the calendar to work
    vm.calendarView = 'month';
    vm.calendarTitle = 'test';
    vm.viewDate = new Date();
    // vm.events = [
    //   {
    //     id: $scope.lastId++,
    //     title: 'An event',
    //     type: 'warning',
    //     startsAt: moment().startOf('week').subtract(2, 'days').add(8, 'hours').toDate(),
    //     endsAt: moment().startOf('week').add(1, 'week').add(9, 'hours').toDate(),
    //     draggable: true,
    //     resizable: true
    //   }, {
    //     id: $scope.lastId++,
    //     title: '<i class="glyphicon glyphicon-asterisk"></i> <span class="text-primary">Another event</span>, with a <i>html</i> title',
    //     type: 'info',
    //     startsAt: moment().subtract(1, 'day').toDate(),
    //     endsAt: moment().add(5, 'days').toDate(),
    //     draggable: true,
    //     resizable: true
    //   }, {
    //     id: $scope.lastId++,
    //     title: 'This is a really long event title that occurs on every year',
    //     type: 'important',
    //     startsAt: moment().startOf('day').add(7, 'hours').toDate(),
    //     endsAt: moment().startOf('day').add(19, 'hours').toDate(),
    //     recursOn: 'year',
    //     draggable: true,
    //     resizable: true
    //   }
    // ];

    // vm.events.total = moment(vm.events.endsAt) - moment(vm.events.startsAt);
    // vm.events.total = moment.duration(vm.events.endsAt.diff(vm.events.startsAt));


    vm.isCellOpen = true;

    vm.eventClicked = function(event) {
      // alert('Clicked');
      console.dir(event);
      $('html, body').animate({
        scrollTop: $("#eventid_"+event.id).offset().top
      }, 400);
      $("#eventid_"+event.id).addClass('active');
    };

    vm.eventEdited = function(event) {
      // alert('Edited');
      console.dir(event);
      $('html, body').animate({
        scrollTop: $("#eventid_"+event.id).offset().top
      }, 400);
      $("#eventid_"+event.id).addClass('active');
    };

    vm.eventDeleted = function(event) {
      // alert('Deleted');
      console.dir(event);
      $('html, body').animate({
        scrollTop: $("#eventid_"+event.id).offset().top
      }, 400);
      $("#eventid_"+event.id).addClass('active');
    };

    vm.eventTimesChanged = function(event) {
      // alert('Dropped or resized');
    };

    vm.toggle = function($event, field, event) {
      $event.preventDefault();
      $event.stopPropagation();
      event[field] = !event[field];
    };

    




  });

myApp.config(function(calendarConfig) {



  

    calendarConfig.displayEventEndTimes = true; //This will display event end times on the month and year views. Default false.



  });
