<div class="container" ng-app="myApp">

<div ng-controller="KitchenSinkCtrl as vm">
<h2 class="text-center">{{ vm.calendarTitle }}</h2>
<h4 class="text-center">Total hours worked this month: {{ vm.totalHours.toFixed(2) }}</h4>

<div class="row">

<div class="col-md-6 text-center">
  <div class="btn-group">

    <button
      class="btn btn-border-primary btn-rounded"
      mwl-date-modifier
      date="vm.viewDate"
      decrement="vm.calendarView">
      Previous
    </button>
    <button
      class="btn btn-border-default"
      mwl-date-modifier
      date="vm.viewDate"
      set-to-today>
      Today
    </button>
    <button
      class="btn btn-border-primary btn-rounded"
      mwl-date-modifier
      date="vm.viewDate"
      increment="vm.calendarView">
      Next
    </button>
  </div>
</div>

<br class="visible-xs visible-sm">

<div class="col-md-6 text-center">
  <div class="btn-group">
    <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'year'">Year</label>
    <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'month'">Month</label>
    <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'week'">Week</label>
    <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'day'">Day</label>
  </div>
</div>

</div>

<br>

<mwl-calendar
events="vm.events"
view="vm.calendarView"
view-title="vm.calendarTitle"
view-date="vm.viewDate"
on-event-click="vm.eventClicked(calendarEvent)"
on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd"
edit-event-html="'<i class=\'glyphicon glyphicon-pencil\'></i>'"
delete-event-html="'<i class=\'glyphicon glyphicon-remove\'></i>'"
on-edit-event-click="vm.eventEdited(calendarEvent)"
on-delete-event-click="vm.eventDeleted(calendarEvent)"
cell-is-open="vm.isCellOpen"
day-view-start="06:00"
day-view-end="22:00"
day-view-split="30"
cell-modifier="vm.modifyCell(calendarCell)">
</mwl-calendar>

<br><br><br>

<h3 id="event-editor">
Edit hours

<button
  class="btn btn-default pull-right"
  ng-disabled="!changed"
  ng-click="saveChanges()">
  Save Changes
</button>
<button
  class="btn btn-primary pull-right"
  ng-click="addNew(event)">
  Add new
</button>
<span
  id="unsaved"
  ng-show="changed"
  class="pull-right small alert alert-info">
  <small>You have unsaved changes!</small>
</span>
<div class="clearfix"></div>
</h3>

<table class="table table-bordered" id="month_view">

<thead>
  <tr>

    <!-- <th>Type</th> -->
    <th>Started</th>
    <th>Ended</th>
    <th>Notes</th>
    <th>Total</th>
    <th>Remove</th>

  </tr>
</thead>

<tbody>
  <tr ng-repeat="event in vm.events | orderBy:'$index':true" ng-class="changeClass(event)" id="eventid_{{event.id}}">
    <!-- <td>
      <select ng-model="event.type" class="form-control">
        <option value="important">Important</option>
        <option value="warning">Warning</option>
        <option value="info">Info</option>
        <option value="inverse">Inverse</option>
        <option value="success">Success</option>
        <option value="special">Special</option>
      </select>
    </td> -->
    <td>
      <p class="input-group" style="max-width: 250px">
        <input
          ng-change="cacheChange(event)"
          type="text"
          class="form-control"
          readonly
          uib-datepicker-popup="dd MMMM yyyy"
          ng-model="event.startsAt"
          is-open="event.startOpen"
          close-text="Close" >
        <span class="input-group-btn">
          <button
            type="button"
            class="btn btn-flat"
            ng-click="vm.toggle($event, 'startOpen', event)">
            <i class="glyphicon glyphicon-calendar"></i>
          </button>
        </span>
      </p>
      <uib-timepicker
        ng-model="event.startsAt"
        ng-change="cacheChange(event)"
        hour-step="1"
        minute-step="15"
        show-meridian="true">
      </uib-timepicker>
    </td>
    <td>
      <p class="input-group" style="max-width: 250px">
        <input
          ng-change="cacheChange(event)"
          type="text"
          class="form-control"
          readonly
          uib-datepicker-popup="dd MMMM yyyy"
          ng-model="event.endsAt"
          is-open="event.endOpen"
          close-text="Close">
        <span class="input-group-btn">
          <button
            type="button"
            class="btn btn-flat"
            ng-click="vm.toggle($event, 'endOpen', event)">
            <i class="glyphicon glyphicon-calendar"></i>
          </button>
        </span>
      </p>
      <uib-timepicker
        ng-model="event.endsAt"
        ng-change="cacheChange(event)"
        hour-step="1"
        minute-step="15"
        show-meridian="true">
      </uib-timepicker>
    </td>
    <td>
      <input
        ng-change="cacheChange(event)"
        type="text"
        class="form-control"
        ng-model="event.title"
        >
    </td>
    <td>
      <input
        type="text"
        class="form-control disabled"
        value="{{ evalDur((event.endsAt - event.startsAt)) }}">
    </td>
    <td>
      <button
        class="btn btn-danger"
        ng-click="deleteEvent(event,$index)"
        ng-hide="event.new">
        Delete
      </button>
    </td>

  </tr>
</tbody>

</table>
</div>
</div>
