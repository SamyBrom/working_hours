<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ideation</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/mdb/mdb.css" rel="stylesheet">
    <link href="bower_components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
  <!-- SideNav slide-out button -->
    <a href="#" data-activates="slide-out" class="navbar-brand button-collapse"><i class="material-icons">menu</i></a>
    <!--/. SideNav slide-out button -->

    <!-- Sidebar navigation -->
    <ul id="slide-out" class="side-nav admin-side-nav dark-side-nav">
        <!-- Logo -->
        <div class="logo-wrapper">
            <img src="http://0.gravatar.com/avatar/60efa32c26a19f3ed2e42798afb705ba?s=100&d=mm&r=g" class="img-responsive img-circle">
            <div class="rgba-stylish-strong">
                <p class="user white-text">Admin
                    <br> admin@gmail.com
                </p>
            </div>
        </div>
        <!--/. Logo -->

        <!-- Side navigation links -->
        <ul class="collapsible collapsible-accordion">
            <li><a href="#intro" class="waves-effect waves-light"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#" class="waves-effect waves-light"><i class="fa fa-money"></i> Sales</a></li>
            <li><a href="#" class="waves-effect waves-light"><i class="fa fa-line-chart"></i> Conversion</a></li>
            <li><a href="#" class="waves-effect waves-light"><i class="fa fa-users"></i> Website Traffic</a></li>
            <li><a href="#" class="waves-effect waves-light"><i class="fa fa-search"></i> SEO</a></li>
            <li><a href="#" class="waves-effect waves-light"><i class="fa fa-share-alt"></i> Social</a></li>
        </ul>
        <!--/. Side navigation links -->

    </ul>
    <!--/. Sidebar navigation -->

    <nav class="double-navbar navbar navbar-fixed-top unique-color z-depth-1" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header pull-left">
                <!-- SideNav slide-out button -->
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
                <!--/. SideNav slide-out button -->

            </div>
            <!-- Navbar Icons -->
            <ul class="list-inline pull-right text-center">
                <li><a href="{{ url('/home') }}" class="waves-effect waves-light"><i class="fa fa-clock-o"></i><br><span>My Times</span></a></li>

                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}" class="waves-effect waves-light"><i class="fa fa-sign-in"></i><br><span>Login</span></a></li>
                    <li><a href="{{ url('/register') }}" class="waves-effect waves-light"><i class="fa fa-clipboard"></i><br><span>Register</span></a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-user"></i><br><span>{{ Auth::user()->name }}</span></a> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li ><a style="color: #666" href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>

            <!--/. Navbar Icons -->
    </div>
</nav>
<!--/.Navbar-->

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="bower_components/moment/moment.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/mdb/mdb.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-touch.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/1.2.0/ui-bootstrap-tpls.min.js"></script>
    <script src="//cdn.rawgit.com/ocombe/ocLazyLoad/1.0.9/dist/ocLazyLoad.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.9.1/highlight.min.js"></script>
    <script src="//cdn.rawgit.com/pc035860/angular-highlightjs/v0.5.1/build/angular-highlightjs.min.js"></script>
    <script src="app.js"></script>
    <!-- {{-- <script src="{{ elixir('js/app.js') }}"></script> --}} -->
</body>
</html>
