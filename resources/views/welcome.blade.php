@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default" style="margin-top: 30px">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Please login/register
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
